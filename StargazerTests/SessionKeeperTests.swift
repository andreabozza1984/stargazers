//
//  SessionKeeper.swift
//  Stargazer
//
//  Created by Andrea Bozza on 29/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import XCTest
import Alamofire

@testable import Stargazer

class SessionKeeperTests: StargazerBaseTests {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /**
     Test of sessionmanegar has been allocated
     */
    func testSessionKeeperAllocated(){
        // Given
        sessionKeeper = SessionKeeper.sharedInstance
        
        // When
        //nothing to do
        
        // Then
        XCTAssertNotNil(sessionKeeper)
    }
    
    /**
     Test if git API is reachable
     */
    
    func testGitHubApiReachable() {
        // Given
        let urlString = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
        XCTAssertNotNil(urlString)
        XCTAssertNotNil(sessionKeeper)
        XCTAssertNotNil(sessionKeeper?.sessionManager)
        // When
        let request = sessionKeeper!.sessionManager!.request(urlString!)
        
        // Then
        XCTAssertNotNil(request.request)
        XCTAssertEqual(request.request?.httpMethod, "GET")
        XCTAssertEqual(request.request?.url?.absoluteString, urlString)
        XCTAssertNil(request.response)
    }
}
