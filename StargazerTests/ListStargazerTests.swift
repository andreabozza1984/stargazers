//
//  StargazerTests.swift
//  StargazerTests
//
//  Created by Andrea Bozza on 25/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import XCTest
import Alamofire

@testable import Stargazer

class ListStargazerTests: StargazerBaseTests {
    var controllerUnderTest : ListStargazerViewController!
    
    override func setUp() {
        super.setUp()
        controllerUnderTest = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Stargazer.Viewcontrollers.LISTOFSTARGAZER) as! ListStargazerViewController
    }
    
    override func tearDown() {
        self.controllerUnderTest = nil
        super.tearDown()
    }
    
    /**
     Test what happen if load more fails
     */
    func testLoadMoreFail(){
        // Given
        var stargazers: [StargazerObject]?
        var errorApi: AFError?
        
        let urlString = "https://api.github.com/repositories/22458259/stargazers?page=2000"
        controllerUnderTest.nextPage = urlString
        let expectation = self.expectation(description: "GET request should succeed: \(urlString)")
        
        // When
        controllerUnderTest.loadMoreData { (objects, error, parameters) in
            stargazers = objects as? [StargazerObject]
            errorApi = error as? AFError
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
        // Then
        XCTAssertNil(stargazers)
        XCTAssertNotNil(errorApi)
        
    }
    
    /**
     Test what happen if load more success
     */
    func testLoadMoreSuccess(){
        // Given
        var stargazers: [StargazerObject]?
        var errorApi: AFError?
        
        let urlString = "https://api.github.com/repositories/22458259/stargazers?page=200"
        controllerUnderTest.nextPage = urlString
        let expectation = self.expectation(description: "GET request should succeed: \(urlString)")
        
        
        // When
        controllerUnderTest.loadMoreData { (objects, error, parameters) in
            stargazers = objects as? [StargazerObject]
            errorApi = error as? AFError
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
        // Then
        XCTAssertNil(stargazers)
        XCTAssertNil(errorApi)
    }
    
    /**
     Test what happen if X-RateLimit-Remaining turn equal to 0
     */
    //Xcode 8 runs the measureBlock code 10 times.
    
    func testPerformance() {
        self.measure() {
            var parameters = [":owner": "Alamofire", ":repo": "Alamofire"]
            for i in 0 ..< 6 {
                // Given
                let urlString = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
                
                let expectation = self.expectation(description: "GET request should succeed: \(urlString)")
                
                var stargazers: [StargazerObject]?
                var errorApi: AFError?
                var remainingRequest = 0
                
                // When
                RemoteDataLoader.getStargazer(parameters) { (objects, error, header) in
                    stargazers = objects as? [StargazerObject]
                    errorApi = error as? AFError
                    remainingRequest = Int(header?["X-RateLimit-Remaining"] as! String)!
                    expectation.fulfill()
                }
                
                self.waitForExpectations(timeout: 10.0, handler: nil)
                
                // Then
                if let apiE = errorApi {
                    if let httpErrorHandlerDict = HttpErrorHandler.errorDispatcher(error: apiE), let code = httpErrorHandlerDict["code"] as? Int{
                        if code == 404 {
                            XCTFail("Query limit reached. Need login to gain 5000 request")
                        }else{
                            XCTAssertNil(errorApi)
                            XCTAssertNotNil(stargazers)
                            
                            XCTAssertNotEqual(remainingRequest, 0, "No more request avaible; try login")
                        }
                    }else{
                        
                        XCTAssertNotEqual(remainingRequest, 0, "No more request avaible; try login")
                    }
                }else{
                    XCTAssertNotNil(stargazers)
                    XCTAssertNotEqual(remainingRequest, 0, "No more request avaible; try login")
                }
                parameters[":repo"] = parameters[":repo"]! + "1"
            }
        }
    }
}
