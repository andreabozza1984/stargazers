//
//  StargazerLoginTests.swift
//  Stargazer
//
//  Created by Andrea Bozza on 29/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import XCTest
import Alamofire

@testable import Stargazer


class StargazerLoginTests: StargazerBaseTests {
    var controllerUnderTest : LoginViewController!
    override func setUp() {
        super.setUp()
        
        controllerUnderTest = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Stargazer.Viewcontrollers.LOGINVIECONTROLLER) as! LoginViewController
        // Put setup code here. This method is called before the invocation of each test method in the class.
      /*  let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginvc = storyboard.instantiateViewController(withIdentifier: Stargazer.Viewcontrollers.LOGINVIECONTROLLER)
        loginvc.*/
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
       controllerUnderTest = nil
        super.tearDown()
 
    }
    
    /**
     Test login success
     */
    func testLoginSuccess(){
        controllerUnderTest.handleLogin(user: "andreabozzasubito", password: "23Andrea84", completion: {
            (logged) -> Void in
           // fulfill()
        })
        
        
       // XCTAssert(logged)
    }
    
    /**
     Test login failed
     */
    func testLoginFail(){
        controllerUnderTest.handleLogin(user: "andreabozza", password: "passIncor", completion: {
            (logged) -> Void in
            XCTAssert(!logged)
        })
    }
    
}
