//
//  StargazerBaseTests.swift
//  Stargazer
//
//  Created by Andrea Bozza on 29/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import Alamofire
import Foundation
import XCTest

//base class. Other XCTest extend it

class StargazerBaseTests: XCTestCase {
    let timeout: TimeInterval = 30.0
    var sessionKeeper: SessionKeeper?
    
    override func setUp() {
        super.setUp()
        sessionKeeper = SessionKeeper.sharedInstance
    }
    
    override func tearDown() {
        sessionKeeper = nil
        super.tearDown()
    }
    
}
