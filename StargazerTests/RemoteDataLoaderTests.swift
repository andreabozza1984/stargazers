//
//  RemoteDataLoaderTests.swift
//  Stargazer
//
//  Created by Andrea Bozza on 29/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import Foundation
import XCTest
import Alamofire

@testable import Stargazer

class RemoteDataLoaderTests: StargazerBaseTests {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /**
     Test fail of stargazer call. Incorrect :repo passed

     */
    func testStargazerWithParamFail() {
        // Given
        let urlString = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
        
        let parameters = [":owner": "Alamofire", ":repo": "AlamoFRIED"]
        
        let expectation = self.expectation(description: "GET request should succeed: \(urlString)")
        
        var stargazers: [StargazerObject]?
        var errorApi: AFError?
        
        // When
        RemoteDataLoader.getStargazer(parameters) { (objects, error, header) in
            stargazers = objects as? [StargazerObject]
            errorApi = error as? AFError
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
        // Then
        XCTAssertNil(stargazers)
        XCTAssertNotNil(errorApi)
    }
    
    /**
     Test success of stargaters call. Correct user and repo
     */

    func testStargazerWithParamSuccess() {
        // Given
        let urlString = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
        
        let parameters = [":owner": "Alamofire", ":repo": "Alamofire"]
        
        let expectation = self.expectation(description: "GET request should succeed: \(urlString)")
        
        var stargazers: [StargazerObject]?
        var errorApi: AFError?
        
        // When
        RemoteDataLoader.getStargazer(parameters) { (objects, error, header) in
            stargazers = objects as? [StargazerObject]
            errorApi = error as? AFError
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
        // Then
        XCTAssertNotNil(stargazers)
        XCTAssertNil(errorApi)
    }
    
    /**
     One time call; test if limit of API call has been reached
     */
    
    func testLimitQuery(){
        // Given
        let urlString = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
        
        let parameters = [":owner": "Alamofire", ":repo": "Alamofire"]
        
        
        let expectation = self.expectation(description: "GET request should succeed: \(urlString)")
        
        var stargazers: [StargazerObject]?
        var errorApi: AFError?
        var remainingRequest = 0
        
        // When
        RemoteDataLoader.getStargazer(parameters) { (objects, error, header) in
            stargazers = objects as? [StargazerObject]
            errorApi = error as? AFError
            remainingRequest = Int(header?["X-RateLimit-Remaining"] as! String)!
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: timeout, handler: nil)
        
        // Then
        XCTAssertNotNil(stargazers)
        
        XCTAssertNil(errorApi)
        
        XCTAssertNotEqual(remainingRequest, 0, "No more requet avaible; try login")
    }
    
}
