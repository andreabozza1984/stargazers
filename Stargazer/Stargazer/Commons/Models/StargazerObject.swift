//
//  Stargazer.swift
//  Stargazer
//
//  Created by Andrea Bozza on 26/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import ObjectMapper

//Mappable deserialized object definition for Stargazer

class StargazerObject: Mappable {
    
    var login : String?
    var id : Int!
    var avatar_url: String? // https://avatars0.githubusercontent.com/u/7659?v=4",
    var gravatar_id: String? // ,
    var url: String? // https://api.github.com/users/mattt",
    var html_url: String? // https://github.com/mattt",
    var followers_url : String? // https://api.github.com/users/mattt/followers",
    var following_url : String? // https://api.github.com/users/mattt/following{/other_user}",
    var gists_url: String? // https://api.github.com/users/mattt/gists{/gist_id}",
    var starred_url : String? // https://api.github.com/users/mattt/starred{/owner}{/repo}",
    var subscriptions_url : String? // https://api.github.com/users/mattt/subscriptions",
    var organizations_url: String? // https://api.github.com/users/mattt/orgs",
    var repos_url: String? // //api.github.com/users/mattt/repos",
    var events_url : String? // https://api.github.com/users/mattt/events{/privacy}",
    var received_events_url : String? //  "https://api.github.com/users/mattt/received_events",
    var type : String? // "User",
    var site_admin : Bool?
    
    required init?(map: Map){
        
    }
    
    //deserialize
    func mapping(map: Map) {
        login <- map["login"]
        id <- map["id"]
        avatar_url <- map["avatar_url"]
        url <- map["url"]
        html_url <- map["html_url"]
        followers_url <- map["followers_url"]
        following_url <- map["following_url"]
        gists_url <- map["gists_url"]
        starred_url <- map["starred_url"]
        subscriptions_url <- map["subscriptions_url"]
        repos_url <- map["repos_url"]
        events_url <- map["events_url"]
        repos_url <- map["repos_url"]
        received_events_url <- map["received_events_url"]
        type <- map["type"]
        site_admin <- map["site_admin"]
    }
    
    
}
