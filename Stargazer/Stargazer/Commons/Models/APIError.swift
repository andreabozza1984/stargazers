//
//  APIError.swift
//  Stargazer
//
//  Created by Andrea Bozza on 27/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import ObjectMapper

//Mappable deserialized object definition for Stargazer
public class APIError: Mappable {
    
    var message: String?
    var documentation_url: String?
    
    required public init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        message <- map["message"]
        documentation_url <- map["documentation_url"]
    }
    
    func getMessage() -> String? {
        return self.message
    }
}
