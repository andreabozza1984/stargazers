//
//  LoggerHelper.swift
//  Stargazer
//
//  Created by Andrea Bozza on 31/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import XCGLogger

class LoggerHelper: NSObject {
    
    var logger : XCGLogger!
    
    
    //singleton
    class var sharedInstance : LoggerHelper {
        struct Static {
            static let instance : LoggerHelper = LoggerHelper()
        }
        return Static.instance
    }
    
    //init session manager
    override init() {
        // create a custom logger configuration
        super.init()
        logger = XCGLogger(identifier: "advancedLogger", includeDefaultDestinations: false)
        self.configure()
    }
    
    private func configure(){
        // Create a destination for the system console log (via NSLog)
        let systemDestination = AppleSystemLogDestination(identifier: "advancedLogger.systemDestination")
        
        // Optionally set some configuration options
        systemDestination.outputLevel = .debug
        systemDestination.showLogIdentifier = false
        systemDestination.showFunctionName = true
        systemDestination.showThreadName = true
        systemDestination.showLevel = true
        systemDestination.showFileName = true
        systemDestination.showLineNumber = true
        systemDestination.showDate = true
        
        // Add the destination to the logger
        logger.add(destination: systemDestination)
        
        // Create a file log destination
        let cacheDirectory: NSURL = {
            let urls = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
            return urls[urls.endIndex - 1] as NSURL
        }()
        let logPath: NSURL = cacheDirectory.appendingPathComponent("app.log")! as NSURL
        let fileDestination = FileDestination(writeToFile: logPath, identifier: "advancedLogger.fileDestination")
        
        // Optionally set some configuration options
        fileDestination.outputLevel = .debug
        fileDestination.showLogIdentifier = false
        fileDestination.showFunctionName = true
        fileDestination.showThreadName = true
        fileDestination.showLevel = true
        fileDestination.showFileName = true
        fileDestination.showLineNumber = true
        fileDestination.showDate = true
        
        // Process this destination in the background
        fileDestination.logQueue = XCGLogger.logQueue
        
        // Add the destination to the logger
        logger.add(destination: fileDestination)
        
        // Add basic app info, version info etc, to the start of the logs
        logger.logAppDetails()
    }

}

