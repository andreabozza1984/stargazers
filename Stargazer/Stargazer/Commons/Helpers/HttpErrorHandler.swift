//
//  HttpErrorHandler.swift
//  Stargazer
//
//  Created by Andrea Bozza on 28/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import Alamofire

//Retrieve status code and fail reason

class HttpErrorHandler: NSObject {
    
    /**
     Retrieve status code and fail reason
     
     - Parameters:
     - error: Error generated from alamofire
     
     - Returns: Dictionary with code and reson if they are presents
     */
    
    static func errorDispatcher(error: AFError)-> [String: Any]?{
        if let error = error as? AFError {
            switch error {
            case .invalidURL(let url):
                LoggerHelper.sharedInstance.logger.debug("Invalid URL: \(url) - \(error.localizedDescription)")
            case .parameterEncodingFailed(let reason):
                LoggerHelper.sharedInstance.logger.debug("Parameter encoding failed: \(error.localizedDescription)")
                LoggerHelper.sharedInstance.logger.debug("Failure Reason: \(reason)")
                return ["reason": reason]
            case .multipartEncodingFailed(let reason):
                LoggerHelper.sharedInstance.logger.debug("Multipart encoding failed: \(error.localizedDescription)")
                LoggerHelper.sharedInstance.logger.debug("Failure Reason: \(reason)")
                return ["reason": reason]
            case .responseValidationFailed(let reason):
                LoggerHelper.sharedInstance.logger.debug("Response validation failed: \(error.localizedDescription)")
                LoggerHelper.sharedInstance.logger.debug("Failure Reason: \(reason)")
                switch reason {
                case .dataFileNil, .dataFileReadFailed:
                    LoggerHelper.sharedInstance.logger.debug("Downloaded file could not be read")
                case .missingContentType(let acceptableContentTypes):
                    LoggerHelper.sharedInstance.logger.debug("Content Type Missing: \(acceptableContentTypes)")
                case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                    LoggerHelper.sharedInstance.logger.debug("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                case .unacceptableStatusCode(let code):
                    LoggerHelper.sharedInstance.logger.debug("Response status code was unacceptable: \(code)")
                    return ["code": code, "reason": reason]
                    
                }
                return ["reason": reason]
            case .responseSerializationFailed(let reason):
                LoggerHelper.sharedInstance.logger.debug("Response serialization failed: \(error.localizedDescription)")
                LoggerHelper.sharedInstance.logger.debug("Failure Reason: \(reason)")
                return ["reason": reason]
            }
            LoggerHelper.sharedInstance.logger.debug("Underlying error: \(error.underlyingError)")
        } else if let error = error as? URLError {
            LoggerHelper.sharedInstance.logger.debug("URLError occurred: \(error)")
        } else {
            LoggerHelper.sharedInstance.logger.debug("Unknown error: \(error)")
        }
        return nil
    }
}
