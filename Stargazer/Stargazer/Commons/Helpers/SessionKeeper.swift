//
//  SessionKeeper.swift
//  Stargazer
//
//  Created by Andrea Bozza on 28/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import Alamofire

class SessionKeeper: NSObject {
    
    var sessionManager : SessionManager?
    
    var headers : HTTPHeaders?
    
    //singleton
    class var sharedInstance : SessionKeeper {
        struct Static {
            static let instance : SessionKeeper = SessionKeeper()
        }
        return Static.instance
    }
    
    //init session manager
    override init() {
        // create a custom session configuration
        let configuration = URLSessionConfiguration.default
        // create a session manager with the configuration
        self.sessionManager = Alamofire.SessionManager(configuration: configuration)
        self.sessionManager?.adapter = MashapeHeadersAdapter()
    }
    
    /**
     Add properly header to urlrequest at every http  call

     */
    
    class MashapeHeadersAdapter: RequestAdapter {
        func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            var urlRequest = urlRequest
            urlRequest.setValue("application/json", forHTTPHeaderField: "Accept")
            if let base64token = UserDefaults.standard.string(forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN){
                urlRequest.setValue("Basic " + base64token, forHTTPHeaderField: "Authorization")
            }
            return urlRequest
        }
    }
    
    
}
