//
//  RemoteDataLoader.swift
//  Stargazer
//
//  Created by Andrea Bozza on 26/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

public typealias ApiResponse = ([Mappable]?, Error?, [AnyHashable : Any]? ) -> Void

fileprivate let token_logging = "RmtDtLer" //log token

// Interacts with remote services
class RemoteDataLoader: NSObject {
    
    /**
     Call GET /repos/:owner/:repo/stargazers
     
     - Parameters:
     - params: Parameter to replace :owner  and :repo
     
     - Returns: List Stargazers.
     */
    class func getStargazer(_ params: [String : String]?, _ handler:@escaping ApiResponse){
        let url =  Stargazer.API.GetStargazers(params)
        SessionKeeper.sharedInstance.sessionManager!.request(
            url,
            method: .get,
            headers: nil)
            .validate(statusCode: 200..<300)
            .validate()
            .responseJSON {
                response in
                switch response.result {
                case .success:
                    LoggerHelper.sharedInstance.logger.debug("Sargazer get : \(params) okay")
                    //let statusCode = (response.response?.statusCode)!
                    let stargazerObjects = Mapper<StargazerObject>().mapArray(JSONObject:response.result.value)
                    handler(stargazerObjects, nil,response.response?.allHeaderFields)
                case .failure(let error):
                    //if message data response.data is not parsable return alamofire error
                    LoggerHelper.sharedInstance.logger.debug("Sargazer get : \(params) fail")
                    guard let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) else {
                        handler(nil, error, response.response?.allHeaderFields)
                        return
                    }
                    let apiError = Mapper<APIError>().map(JSONObject:json)
                    if let apiError = apiError {
                        handler([apiError], error, response.response?.allHeaderFields)
                    }else{
                        handler(nil, error, response.response?.allHeaderFields)
                    }
                }
        }
    }
    
    /**
     Call url similar to GET https://api.github.com/repositories/22458259/stargazers?page=820
     
     - Parameters:
     - nextPageUrl: next page results
     
     - Returns: List Stargazers.
     */
    class func getStargazerPaginated(_ nextPageUrl: String, _ handler:@escaping ApiResponse){
        let url =  nextPageUrl
        SessionKeeper.sharedInstance.sessionManager!.request(
            url,
            method: .get,
            headers: nil)
            .validate(statusCode: 200..<300)
            .validate()
            .responseJSON {
                response in
                switch response.result {
                case .success:
                    LoggerHelper.sharedInstance.logger.debug("Pagination get : \(nextPageUrl) okay")
                    //let statusCode = (response.response?.statusCode)!
                    //print("...HTTP code: \(statusCode)")
                    let stargazerObjects = Mapper<StargazerObject>().mapArray(JSONObject:response.result.value)
                    handler(stargazerObjects, nil,response.response?.allHeaderFields)
                case .failure(let error):
                    LoggerHelper.sharedInstance.logger.debug("Pagination get : \(nextPageUrl) fail")
                    //if message data response.data is not parsable return alamofire error
                    guard let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) else {
                        handler(nil, error, response.response?.allHeaderFields)
                        return
                    }
                    let apiError = Mapper<APIError>().map(JSONObject:json)
                    if let apiError = apiError {
                        handler([apiError], error, response.response?.allHeaderFields)
                    }else{
                        handler(nil, error, response.response?.allHeaderFields)
                    }
                }
        }
    }
    
    /**
     Call GET /users/:username
     
     - Parameters:
     - params: Parameter to replace :username
     
     - Returns: User information.
     */
    
    class func getUserInfo(_ params: [String : String]?, _ handler:@escaping ApiResponse){
        let url =  Stargazer.API.GetUserInfo(params)
        SessionKeeper.sharedInstance.sessionManager!.request(
            url,
            method: .get,
            headers: nil)
            .validate(statusCode: 200..<300)
            .validate()
            .responseJSON {
                response in
                switch response.result {
                case .success:
                    LoggerHelper.sharedInstance.logger.debug("Userinfo get : \(params) okay")
                    //let statusCode = (response.response?.statusCode)!
                    let stargazerObjects = Mapper<StargazerObject>().map(JSONObject:response.result.value)
                    handler([stargazerObjects!], nil,response.response?.allHeaderFields)
                case .failure(let error):
                    LoggerHelper.sharedInstance.logger.debug("Userinfo get : \(params) fail")
                    //if message data response.data is not parsable return alamofire error
                    guard let json = try? JSONSerialization.jsonObject(with: response.data!, options: []) else {
                        handler(nil, error, response.response?.allHeaderFields)
                        return
                    }
                    let apiError = Mapper<APIError>().map(JSONObject:json)
                    if let apiError = apiError {
                        handler([apiError], error, response.response?.allHeaderFields)
                    }else{
                        handler(nil, error, response.response?.allHeaderFields)
                    }
                }
        }
    }
}
