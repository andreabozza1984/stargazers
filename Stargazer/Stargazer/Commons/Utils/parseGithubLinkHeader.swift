
import Foundation

/* Parse the Github Link HTTP header used for pagination
 * https://developer.github.com/v3/#link-header
 * Swift
 * Ported from: https://gist.github.com/niallo/3109252
 */
func parseGithubLinkHeader(input: String) -> [String: String] {
    guard (input.characters.count != 0) else {
        fatalError("input must not be of zero length")
    }
    
    // Split parts by comma
    let parts = input.components(separatedBy: ",")
    var links = [String: String]()
    // Parse each part into a named link
    for p in parts {
        var section = p.components(separatedBy: ";")
        guard section.count == 2 else {
            fatalError("section could not be split on ';'")
        }
        
        let urlRegex = try! NSRegularExpression(pattern: "<(.*)>", options: [])
        let url = urlRegex.stringByReplacingMatches(in: section[0], options: [], range: NSRange(location: 0, length: section[0].characters.count), withTemplate: "$1").trimmingCharacters(in: CharacterSet.init(charactersIn: " "))
        
        let nameRegex = try! NSRegularExpression(pattern: "rel=\"(.*)\"", options: [])
        let name = nameRegex.stringByReplacingMatches(in: section[1], options: [], range: NSRange(location: 0, length: section[1].characters.count), withTemplate: "$1").trimmingCharacters(in: CharacterSet.init(charactersIn: " "))
        
        links[name] = url;
    }
    
    return links;
}
