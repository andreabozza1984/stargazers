//
//  Stargazer.swift
//  Stargazer
//
//  Created by Andrea Bozza on 26/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import Toast_Swift

/// struct used to handle constant and other app aspect (color, font etc.)
public struct Stargazer{
    
    struct Constants {
        
        private init(){} // hide
        static let CONSTANT_SOME               = "some"
        
    }
    
    struct Notification {
        
        private init(){} // hide
        static let UPDATE_UI               = "updateUi"
        
    }
    
    
    struct UserDefaultValues {
        
        private init(){} // hide
        static let CONSTANT_KBASICB64TOKEN               = "CONSTANT_KBASICB64TOKEN"
    }
    
    struct Segues {
        
        private init(){} // hide
        static let HISTORY               = "showHistoryID"
        
    }
    
    struct Viewcontrollers {
        private init(){} // hide
        static let LOGINVIECONTROLLER   = "LoginViewControllerID"
        static let LISTOFSTARGAZER   = "ListofstargazersID"
    }
    
    struct Cells {
        private init(){} // hide
        static let STARGAZERCELLID               = "listStargazerCellTableViewCellID"
        
    }
    /**
     Retrieve string from a key contained inside plist
     
     - Parameters:
     - aKey: Parameter to replace :owner  and :repo
     
     - Returns: Return String from plist.
     */
    
    static func getKeyFromInfodictionary(aKey: String) -> String?{
        //TODO: plist contains different type of data not olny string
        let aValue = Bundle.main.infoDictionary![aKey] as? String
        if let aValue = aValue {
            return aValue
        }else{
            return nil
        }
    }
    
    /**
     Replace a placeholder keys (es. :key) with value contained inside dictionary
     
     - Parameters:
     - params: Dictionary with key and value
     
     - Returns: Return String with placeholder replaced.
     */
    static func replaceValues ( str: String, dict: Dictionary<String, String>) -> String {
        var str = str
        for (key, value) in dict {
            str = str.replacingOccurrences(of: key, with: value)
        }
        return str
    }
    
    /**
     Return if user is logged
     
     - Returns: True if is logged (base64token stored).
     */
    
    static func isLogged () -> Bool {
        if let _ = UserDefaults.standard.string(forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN){
            return true
        }else{
            return false
        }
    }
    
    /**
     Show message toast. While toast is being displayed, avoid touches so that toasts dont accumulate in the queue
     
     - Parameters:
     - txt: txt
     - err: error message
     */
    //TODO: While toast is being displayed, avoid touches so that toasts dont accumulate in the queue
    
    static func showToastOnWindow(_ txt: String, andError err:Error?){
        let app = UIApplication.shared
        let window = app.keyWindow!
        let duration = 2
        if let err = err {
            let txtString = txt + ": " + err.localizedDescription
            window.makeToast(txtString, duration: TimeInterval(duration), position: .center)
        }else {
            window.makeToast(txt, duration: TimeInterval(duration), position: .center)
        }
    }
}
