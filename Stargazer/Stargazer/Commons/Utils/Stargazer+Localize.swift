//
//  Stargazer+Localize.swift
//  Stargazer
//
//  Created by Andrea Bozza on 30/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit

/// extension used to handle configuration, error message, notification etc
import Foundation

extension Stargazer{
    struct Localize {
        
        // Follows format KEY.localizedWithComment(COMMENT)
        // Further down using the extension, we can switch tables, or multiple character set for the purpose of debug
        
        // STARGAZER
        static let searchButton = NSLocalizedString("search_button", comment: "Search button in stargazer list")
        static let clearButton = NSLocalizedString("clear_button", comment: "Clear seach button in stargazer list")
        static let owner = NSLocalizedString("owner", comment: "Textfield owner placeholder in stargazer list")
        static let repo = NSLocalizedString("repo", comment: "Textfield repo placeholder in stargazer list")
        static let loginButton = NSLocalizedString("login_button", comment: "login button in stargazer list")
        static let logoutButton = NSLocalizedString("logout_button", comment: "logout button in stargazer list")
        static let requestAvaible = NSLocalizedString("requestavaible", comment: "request avaible label in stargazer list")
        static let loadedResults = NSLocalizedString("loadresults", comment: "load results label in stargazer list")
        
        //LOGIN
        static let signinButton = NSLocalizedString("signin_button", comment: "button signin in login ")
        
        static let passwordGithubPlaceholder = NSLocalizedString("password_github_placeholder", comment: "passowrd textfield placeholder in login")
        
        static let usernameGithubPlaceholder = NSLocalizedString("username_github_placeholder", comment: "username textfield placeholder in login")
        
        //MESSAGE
        
        static let ownertextfieldEmpty = NSLocalizedString("ownertextfield_empty", comment: "Toast owner texfield empty")
        
        static let repotextfieldEmpty = NSLocalizedString("repotextfield_empty", comment: "Toast repository texfield empty")
        static let searchProblem = NSLocalizedString("search_problem", comment: "Some error encounterd during search")
        
        
        static let passwordEmpty  = NSLocalizedString("password_empty", comment: "Toast owner texfield empty")
        
        static let usernameEmpty = NSLocalizedString("username_empty", comment: "Toast owner texfield empty")
        
        static let loginSuccess = NSLocalizedString("login_success", comment: "Login correct; user authenticated")
        
        static let loginFail = NSLocalizedString("login_fail", comment: "Login problem; user not authenticated")
        
        
        // COMMON
        static let na = NSLocalizedString("notavaible", comment: "Generic not avaible")
    }
}
