//
//  Stargazer+API.swift
//  Stargazer
//
//  Created by Andrea Bozza on 26/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit

/// extension used to handle configuration, error message, notification etc
import Foundation

extension Stargazer {
    struct API{
        struct ResponseCodes {
        }
        
        /**
         Retrieve path of service stargarzers with parameters
         
         - Parameters:
         - params: Parameter to replace :owner  and :repo
         
         - Returns: Return String url of Stargazers API.
         */
        static func GetStargazers(_ params: [String: String]?)-> String{
            let base = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
            if let base = base {
                var api = "repos/:owner/:repo/stargazers"
                if let parameters = params {
                    api =  Stargazer.replaceValues(str: api, dict: parameters)
                }
                return base + api
            }else{
                fatalError("Check your configuration file.plist")
            }
        }
        
        
        /**
         Retrieve path of service users with parameters
         
         - Parameters:
         - params: Parameter to replace :username
         
         - Returns: Return String url of users API.
         */
        static func GetUserInfo(_ params: [String: String]?)-> String{
            let base = Stargazer.getKeyFromInfodictionary(aKey: "basePath")
            if let base = base {
                var api = "users/:username"
                if let parameters = params {
                    api =  Stargazer.replaceValues(str: api, dict: parameters)
                }
                return base + api
            }else{
                fatalError("Check your configuration file.plist")
            }
        }
        
    }
}
