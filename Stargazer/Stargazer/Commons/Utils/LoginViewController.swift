//
//  LoginViewController.swift
//  Stargazer
//
//  Created by Andrea Bozza on 28/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import Alamofire
import FirebaseAnalytics
import ICDMaterialActivityIndicatorView
import SnapKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    var loader: ICDMaterialActivityIndicatorView!
    
    var username : String?
    
    var password : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Login"
        self.userNameTextField.placeholder = Stargazer.Localize.usernameGithubPlaceholder
        self.passwordTextField.placeholder = Stargazer.Localize.passwordGithubPlaceholder
        self.loader = ICDMaterialActivityIndicatorView(activityIndicatorStyle: ICDMaterialActivityIndicatorViewStyleMedium)
        self.loader.hidesWhenStopped = true
        self.view.addSubview(self.loader)
        
        self.loader.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: action
    
    @IBAction func signinAction(_ sender: Any) {
        if (userNameTextField.text?.isEmpty)!{
            Stargazer.showToastOnWindow(Stargazer.Localize.usernameEmpty, andError: nil)
            return
        }
        
        if userNameTextField.isFirstResponder {
            userNameTextField.resignFirstResponder()
        }
        
        
        if (passwordTextField.text?.isEmpty)!{
            Stargazer.showToastOnWindow(Stargazer.Localize
                .passwordEmpty, andError: nil)
            return
        }
        
        if passwordTextField.isFirstResponder {
            passwordTextField.resignFirstResponder()
        }
        
        self.loader.startAnimating()
        self.handleLogin(user: userNameTextField.text!, password: passwordTextField.text!, completion: {
            (logged) -> Void in
             self.loader.stopAnimating()
            if logged {
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
    
    //MARK: textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isFirstResponder {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    //MARK: private function
    
    /**
     Try a call with athentication BASIC
     
     - Parameters:
     - params: Parameter to replace
     - user: user name
     - password: password of user
     
     */
    
    func handleLogin(user: String, password: String, completion: ((_ logged : Bool) -> Void)?){
        
        let userdefaults = UserDefaults.standard
        let stringForBasicAuth: String = String(format: "%@:%@",user, password) as String
        let plainData = stringForBasicAuth.data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: [])
        if let base64String = base64String {
            
            userdefaults.set(base64String, forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN)
            userdefaults.synchronize()
            
            RemoteDataLoader.getUserInfo([":username": user]) { (objects, error, header) in
                if let error = error{
                    //possible error 401 (unathorized) and 404 (user not exist)
                    LoggerHelper.sharedInstance.logger.debug("login fail")
                    userdefaults.removeObject(forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN)
                    userdefaults.synchronize()
                    if let objects = objects{
                        let  apiError = objects.first as! APIError
                        if let message = apiError.message {
                            Stargazer.showToastOnWindow(message, andError: nil)
                        }
                    }else{
                        Stargazer.showToastOnWindow(Stargazer.Localize.loginFail, andError: error)
                    }
                    completion!(false)
                }else{
                    LoggerHelper.sharedInstance.logger.debug("login success")
                    Analytics.logEvent(AnalyticsEventLogin, parameters: [
                        AnalyticsParameterSignUpMethod : "GitHub"])
                    Stargazer.showToastOnWindow(Stargazer.Localize.loginSuccess, andError: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: Stargazer.Notification.UPDATE_UI), object: nil)
                    completion!(true)
                    
                }
            }
        }
        
    }
}
