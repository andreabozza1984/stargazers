//
//  ListStargazerCellTableViewCell.swift
//  Stargazer
//
//  Created by Andrea Bozza on 27/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import AlamofireImage

class ListStargazerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: data setter
    
    /**
     Prepare cell UI with stargazer object
     
     - Parameters:
     - stargazer:  A stargazers object list
     
     */
    
    func prepareItemTableCell(stargazer: StargazerObject){
        self.userNameLabel.text = stargazer.login
        if let avatar = stargazer.avatar_url {
            let url = URL(string: avatar)!
            let placeholderImage = UIImage(named: "placeholder")!
            let filter = AspectScaledToFillSizeWithRoundedCornersFilter(
                size: self.avatarImageView.frame.size,
                radius: 20.0
            )
            self.avatarImageView.af_setImage(  withURL: url,
                                               placeholderImage: placeholderImage,
                                               filter: filter,
                                               imageTransition: .crossDissolve(0.2))
        }else{
            //placeholder
        }
    }
    
}
