//
//  ListStargazerViewController.swift
//  Stargazer
//
//  Created by Andrea Bozza on 27/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper
import FirebaseAnalytics
import ICDMaterialActivityIndicatorView
import SnapKit

/// list of stargazer selected with given owenr and repository name
class ListStargazerViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var ownerTextField: UITextField!
    
    @IBOutlet weak var repoTextfield: UITextField!
    
    @IBOutlet weak var stargazerListTableView: UITableView!
    
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var remainingOperationLabel: UILabel! //remaining call to API; debug purpose
    
    @IBOutlet weak var resetSearchField: UIButton!
    
    var loader: ICDMaterialActivityIndicatorView!
    
    var stargazer = [StargazerObject]() //array of stargazer
    
    var loadingData = false //boolean indicationg if more data fuction is processing
    
    var nextPage : String? //next page to call for load data
    
    var lastPage : String? //last result page
    
    var prevPage : String? //prev result page
    
    var remainingRequest =  Stargazer.Localize.na //remaining git hub api request
    
    var totalRequest =  Stargazer.Localize.na //total git hub api request
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.accessibilityIdentifier = "stargazerList"
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: NSNotification.Name(rawValue: Stargazer.Notification.UPDATE_UI), object: nil)
        self.loader = ICDMaterialActivityIndicatorView(activityIndicatorStyle: ICDMaterialActivityIndicatorViewStyleMedium)
        self.loader.hidesWhenStopped = true
        self.title = "Stargazer"
        self.ownerTextField.placeholder = Stargazer.Localize.owner
        self.repoTextfield.placeholder = Stargazer.Localize.repo
        self.buttonSearch.setTitle(Stargazer.Localize.searchButton, for: .normal)
        self.resetSearchField.setTitle(Stargazer.Localize.clearButton, for: .normal)
        self.updateUI()
        
        self.view.addSubview(self.loader)
        
        self.loader.snp.makeConstraints({ (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        })
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Stargazer.Notification.UPDATE_UI), object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: tableview delegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell  = tableView.dequeueReusableCell(withIdentifier: Stargazer.Cells.STARGAZERCELLID, for: indexPath as IndexPath) as! ListStargazerTableViewCell
        cell.prepareItemTableCell(stargazer: self.stargazer[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        LoggerHelper.sharedInstance.logger.debug("Implement details for stargazer")
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = stargazer.count - 1
        if !loadingData && indexPath.row == lastElement {
            self.loadingData = true
            self.loadMoreData({_,_,_ in })
        }
    }
    
    //MARK: tableview datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stargazer.count
    }
    
    //MARK: textfield delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.isFirstResponder {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    //MARK: ibaction and uiaction
    
    /**
     Reset button ownerTextfield and repoTextField
     
     - Parameters:
     - sender: resetSearchFieldButton
     
     */
    @IBAction func clearSearchFields(_ sender: Any) {
        self.ownerTextField.text = ""
        self.repoTextfield.text = ""
    }
    
    /**
     Launch search action
     
     - Parameters:
     - sender: buttonSearch
     
     */
    
    @IBAction func searchRepoAndUserStargazerAction(_ sender: Any) {
        /*
         UserDefaults.standard.set("fdfdf", forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN)
         UserDefaults.standard.synchronize()
         */
        if (ownerTextField.text?.isEmpty)!{
            Stargazer.showToastOnWindow(Stargazer.Localize.ownertextfieldEmpty, andError: nil)
            return
        }
        
        if ownerTextField.isFirstResponder {
            ownerTextField.resignFirstResponder()
        }
        
        
        if (repoTextfield.text?.isEmpty)!{
            Stargazer.showToastOnWindow(Stargazer.Localize.repotextfieldEmpty, andError: nil)
            return
        }
        
        if repoTextfield.isFirstResponder {
            repoTextfield.resignFirstResponder()
        }
        
        Analytics.logEvent(AnalyticsEventSearch, parameters: [
            AnalyticsParameterSearchTerm: "owner:\(ownerTextField.text!) repo: \(repoTextfield.text!)"
            ])
        self.removeLastSearch()
        
        self.updateUI()
        self.loader.startAnimating()
        RemoteDataLoader.getStargazer([":owner": ownerTextField.text!, ":repo": repoTextfield.text!]) { (objects, error, header) in
            self.loader.stopAnimating()
            self.handleStargazerListError(error: error as? AFError, objects: objects, header:  header, { (stargazers, error, header) in
            })
        }
        
    }
    
    /**
     Call LoginViewController
     
     */
    @objc private func loginAction(){
        if Stargazer.isLogged(){
            UserDefaults.standard.removeObject(forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN)
            UserDefaults.standard.synchronize()
            self.updateUI()
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loginvc = storyboard.instantiateViewController(withIdentifier: Stargazer.Viewcontrollers.LOGINVIECONTROLLER)
            self.navigationController?.pushViewController(loginvc, animated: true)
        }
    }
    
    //MARK: function
    
    /**
     Load more data with url contained in header attribute LINK. Also retrieve the remaing API call usage and limit
     
     */
    func loadMoreData(_ handler:@escaping ApiResponse){
        if let nextPage = self.nextPage {
            LoggerHelper.sharedInstance.logger.debug("load more data at url=\(nextPage)")
            self.loadingData = false
            RemoteDataLoader.getStargazerPaginated(nextPage) { (stargazers, error, header) in
                self.handleStargazerListError(error: error as? AFError, objects: stargazers, header: header, { (stargazers, error, header) in
                    handler(stargazers, error, header)
                })
            }
        }else{
            LoggerHelper.sharedInstance.logger.debug("load more data reached")
        }
    }
    
    /**
     Update UI with new data
     
     */
    @objc private func updateUI(){
        let queriesAvaible = "\(Stargazer.Localize.requestAvaible):" + self.remainingRequest  + "/" + self.totalRequest
        let loadedResult = "\(Stargazer.Localize.loadedResults):\(self.stargazer.count)"
        self.remainingOperationLabel.text =  queriesAvaible + " " + loadedResult
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(title: Stargazer.isLogged() == true ? Stargazer.Localize.logoutButton : Stargazer.Localize.loginButton , style: .done, target: self, action: #selector(loginAction))]
    }
    
    /**
     Remove last search data
     
     */
    private func removeLastSearch(){
        self.stargazer.removeAll()
        self.stargazerListTableView.reloadData()
        self.nextPage = nil
        self.loadingData = false //boolean indicationg if more data fuction is processing
        self.lastPage = nil
        self.prevPage = nil
        self.remainingRequest = Stargazer.Localize.na //remaining git hub api request
        self.totalRequest = Stargazer.Localize.na //total git hub api request
    }
    
    /**
     Call GET /repos/:owner/:repo/stargazers
     
     - Parameters:
     - LinkHeader: contains next url to call to retrieve new users
     
     - Returns: Next url to process.
     */
    
    private func extractNextPage(linkHeader: String)->String?{
        //handle no more page
        let parsed = parseGithubLinkHeader(input: linkHeader)
        if let next = parsed["next"] {
            return next
        }else{
            return nil
        }
    }
    
    /**
     Handle failed response
     
     - Parameters:
     - error: Alamofire error objec
     - objects: Array; in this case contains mapped error message
     - header: header returned in response
     
     */
    
    private func handleStargazerListError(error: AFError?, objects: [Mappable]?, header:  [AnyHashable : Any]?, _ handler:@escaping ApiResponse){
        if let error = error {
            //handle error
            var statusCode = 0
            if let httpErrorHandlerDict = HttpErrorHandler.errorDispatcher(error: error), let code = httpErrorHandlerDict["code"] as? Int{
                //401 if user encounter some problem with authentication notify it
                statusCode = code
            }
            
            if let objects = objects{
                let  apiError = objects.first as! APIError
                if let message = apiError.message {
                    if statusCode == 401 {
                        Stargazer.showToastOnWindow(message + "(\(statusCode))", andError: nil)
                        UserDefaults.standard.removeObject(forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN)
                        UserDefaults.standard.synchronize()
                        self.updateUI()
                    }else{
                        Stargazer.showToastOnWindow(message, andError: nil)
                    }
                }
            }else{
                Stargazer.showToastOnWindow(Stargazer.Localize.searchProblem, andError: error)
            }
        }else{
            self.stargazer.append(contentsOf: objects as! [StargazerObject])
            self.prevPage =  self.nextPage
            self.nextPage = self.extractNextPage(linkHeader: header?["Link"] as! String)
            self.remainingRequest = header?["X-RateLimit-Remaining"] as! String
            self.totalRequest = header?["X-RateLimit-Limit"] as! String
            //TODO: user test fail in loadmore if ui not allocated
            if self.stargazerListTableView != nil {
                self.stargazerListTableView.reloadData()
                self.updateUI()
            }
        }
        handler(objects, error, header)
    }
    
}
