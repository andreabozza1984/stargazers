![Stargazer](urlImage)

## Stargazer

`Stargazer` is a simple iOS app written in [Swift](https://developer.apple.com/swift/).

### Features
- [x] Get list of Stargazers from github [API](https://api.github.com/repos/:owner/:repo/stargazers).
- [x] Login with BASIC ATHENTICATION
- [x] XCTEST and UI TEST
- [x] Firebase integration
- [x] Localization enable


## Installation

### CocoaPods

`Stargazer` contains this [CocoaPods](http://cocoapods.org/).

- 'SnapKit', '3.0.2' :
- 'Alamofire', '4.5.0' :
- 'ObjectMapper', '2.2.8' : 
- 'AlamofireImage', '3.2.0' :
- 'Firebase/Core', '4.0.4' :
- 'Firebase/Crash', '4.0.4' :
- 'Toast-Swift', '2.0.0' :
- 'Material', '2.1.2' :
- 'GoogleMaterialIconFont', '0.5.0' :
- 'FLEX', '2.4.0', :configurations => ['Debug'] :
- 'LayoutKit', '3.0.0' :
- 'ReachabilitySwift', '3' :
- 'ICDMaterialActivityIndicatorView', '0.1.3' :
- 'XCGLogger', '5.0.1' :

Use `pod install` if you clone project 

## Manual

#Success case stargazers
In first view do this action:

- write "alamofire" in firtst textfield
- write "alamofire" in second textfield
- press search
- table populate with stargazers data

#Fail case stargazers
In first view do this action:

- write "alamofire" in firtst textfield
- write "alamofRIED" in second textfield
- press search
- Error occurs

#Fail case remaining request expires

In first view do this action:
- write "alamofire" in firtst textfield
- write "alamofire" in second textfield
- press search
- scroll until Avaible remaining request turns to 0
- do Login

#Success case login
In first view do this action:

- press login button (right button in navigation bar)
- write "andreabozzasubito" in firtst textfield
- write "23Andrea84" in second textfield
- press login
- login occurs; retry a Stargazer's search

#Fail case login
In first view do this action:

- press login button (right button in navigation bar)
- write "andreabozzasubito" in firtst textfield
- write "incorrectpw" in second textfield
- press search
- login fails

##Device use for test
- iphone 7 iOS 10.3.3
- ipad MD788L/B iOS 10.3.3

##Firebase

Create you own GoogleService-Info.plist to test firebase service

## TODO

- Better logging
- Create a history view with coredata to retry search
- Create a Strongest oauth2 athenication Server-side and Client side

## Resources

* [Github API](https://developer.github.com/v3/)
* [iOS Unit Testing and UI Testing Tutorial](https://www.raywenderlich.com/150073/ios-unit-testing-and-ui-testing-tutorial)
* [Firebase](https://firebase.google.com)

## License

`Stargazer` is available under the MIT license.