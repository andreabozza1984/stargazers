//
//  StargazerUITestBase.swift
//  Stargazer
//
//  Created by Andrea Bozza on 30/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import XCTest

class StargazerUITestBase: XCTestCase {
    
    var app : XCUIApplication!
    override func setUp() {
        super.setUp()
        app = XCUIApplication()
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        XCUIApplication().launch()

        //SEE in main; if it is needed to reset some state (es. clear login userdefaults)
        app.launchArguments.append("--uitesting")
    }
}
/**
 Extension that help to find last element visible and scroll to it in a tableview
 */
extension XCUIElement {
    func scrollToElement(element: XCUIElement) {
        while !element.visible() {
            swipeUp()
        }
    }
    
    func visible() -> Bool {
        guard self.exists && !self.frame.isEmpty else { return false }
        return XCUIApplication().windows.element(boundBy: 0).frame.contains(self.frame)
    }
}
