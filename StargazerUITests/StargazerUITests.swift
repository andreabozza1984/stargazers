//
//  StargazerUITests.swift
//  StargazerUITests
//
//  Created by Andrea Bozza on 25/07/2017.
//  Copyright © 2017 Andrea Bozza. All rights reserved.
//

import XCTest

class StargazerUITests: StargazerUITestBase {
    //SEE: https://stackoverflow.com/questions/32184837/ui-testing-failure-neither-element-nor-any-descendant-has-keyboard-focus-on-se
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    /**
     Test when search return results
     */
    func testSearchSuccess() {
        let ownerTextField = app.textFields["Owner"]
        let repoTextField = app.textFields["Repo"]
        ownerTextField.tap()
        ownerTextField.typeText("Alamofire")
        repoTextField.tap()
        repoTextField.typeText("Alamofire")
        app.buttons["search"].tap()
        let tablesQuery = app.tables
        let count = tablesQuery.cells.count
        XCTAssert(count > 0)
    }
    
    /**
     Test when search return no results
     */
    func testSearchFail() {
        let ownerTextField = app.textFields["Owner"]
        let repoTextField = app.textFields["Repo"]
        ownerTextField.tap()
        ownerTextField.typeText("Alamofire")
        repoTextField.tap()
        repoTextField.typeText("AlamofRIED")
        app.buttons["search"].tap()
        let tablesQuery = app.tables
        let count = tablesQuery.cells.count
        XCTAssert(count == 0)
    }
    
    /**
     Test when login work correctly
     */
    func testLoginSuccess(){
        app.launch()
        app.navigationBars["Stargazer"].buttons["Login"].tap()
        let ownerTextField = app.textFields["Github username"]
        let repoTextField = app.secureTextFields["Github passowrd"]
        ownerTextField.tap()
        ownerTextField.typeText("andreabozzasubito")
        repoTextField.tap()
        repoTextField.typeText("23Andrea84")
        app.buttons["signin"].tap()
        XCTAssert(app.navigationBars["Stargazer"].exists)
    }
    
    /**
     Test when login KO
     */
    func testLoginFail(){
        app.launch()
        app.navigationBars["Stargazer"].buttons["Login"].tap()
        let ownerTextField = app.textFields["Github username"]
        let repoTextField = app.secureTextFields["Github passowrd"]
        ownerTextField.tap()
        ownerTextField.typeText("andreabozzasubito")
        repoTextField.tap()
        repoTextField.typeText("passworderrata")
        app.buttons["signin"].tap()
        XCTAssert(app.navigationBars["Login"].exists)
    }
    
    
    //surpass 15.0 of timeout
    /*
     /**
     Scroll all element of stargazer' table
     */
    func testScrollDown(){
        let ownerTextField = app.textFields["Owner"]
        let repoTextField = app.textFields["Repo"]
        let userdefaults = UserDefaults.standard
        let stringForBasicAuth: String = String(format: "%@:%@","andreabozzasubito", "23Andrea84") as String
        let plainData = stringForBasicAuth.data(using: String.Encoding.utf8)
        let base64String = plainData?.base64EncodedString(options: [])
        if let base64String = base64String {
            userdefaults.set(base64String, forKey: Stargazer.UserDefaultValues.CONSTANT_KBASICB64TOKEN)
            userdefaults.synchronize()
        }else{
            XCTFail("problem in storing data for BASIC AUTHENTICATION")
        }
        ownerTextField.tap()
        ownerTextField.typeText("Alamofire")
        repoTextField.tap()
        repoTextField.typeText("Alamofire")
        app.buttons["search"].tap()
        let table = app.tables.element(boundBy: 0)
        let lastCell = table.cells.element(boundBy: table.cells.count-1)
        table.scrollToElement(element: lastCell)
        waitForExpectations(timeout: 120, handler: nil)
    }
    */
}


