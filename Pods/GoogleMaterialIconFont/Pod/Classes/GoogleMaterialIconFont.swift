//
//  GoogleMaterialIconFont.swift
//  Pods
//
//  Created by Yusuke Kita on 9/23/15.
//
//

import UIKit

public extension String {
    public static func materialIcon(_ font: MaterialIconFont) -> String {
        return IconFont.codes[font.rawValue]
    }
}

public extension NSString {
    public static func materialIcon(_ font: MaterialIconFont) -> NSString {
        return NSString(string: String.materialIcon(font))
    }
}

public extension DispatchQueue {
    
    fileprivate static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(_ token: String, block:(Void)->Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}

public extension UIFont {
    public static func materialIconOfSize(_ size: CGFloat) -> UIFont {
        let _onceToken = UUID().uuidString
        let filename = "MaterialIcons-Regular"
        let fontname = "Material Icons"

        if UIFont.fontNames(forFamilyName: fontname).count == 0 {
            DispatchQueue.once(_onceToken) {
                FontLoader.loadFont(filename)
            }
        }
        return UIFont(name: fontname, size: size)!
    }
}

private class FontLoader {
    class func loadFont(_ name: String) {
        let bundle = Bundle(for: FontLoader.self)
        let identifier = bundle.bundleIdentifier
        let fileExtension = "ttf"
        
        let fontURL: URL
        if identifier?.hasPrefix("org.cocoapods") == true {
            fontURL = bundle.url(forResource: name, withExtension: fileExtension, subdirectory: "GoogleMaterialIconFont.bundle")!
        } else {
            fontURL = bundle.url(forResource: name, withExtension: fileExtension)!
        }
        
        let data = try? Data(contentsOf: fontURL)
        let provider = CGDataProvider(data: data! as CFData)
        let font = CGFont(provider!)
        
        var error: Unmanaged<CFError>?
        if !CTFontManagerRegisterGraphicsFont(font, &error) {
            let errorDescription: CFString = CFErrorCopyDescription(error!.takeUnretainedValue())
            let nsError = error!.takeUnretainedValue() as AnyObject as! NSError
            NSException(name: NSExceptionName.internalInconsistencyException, reason: errorDescription as String, userInfo: [NSUnderlyingErrorKey: nsError]).raise()
        }
    }
}
